# -*- coding: utf-8 -*-
from xml.dom import minidom
tiedosto = 'UNIdead.ukml'
print "Luetaan muistiin UKML-tiedosto ", tiedosto, "..."
doc = minidom.parse(tiedosto)

keylist = doc.getElementsByTagName('key')
keylist_pituus = len(keylist)
print keylist_pituus, " näppäintä määritelty."

deadkeylist = doc.getElementsByTagName('deadkey')
deadkeylist_pituus = len(deadkeylist)
print deadkeylist_pituus, " kuollutta näppäintä määritelty."
print ""

for key in range(0, keylist_pituus):
    statelist = keylist[key].getElementsByTagName('state')
    statelist_pituus = len(statelist)
    paikka = keylist[key].getAttribute('position')
    virtuaalikoodi = keylist[key].getAttribute('code')
    print "paikka = ", paikka
    print "virtuaalikoodi = ", virtuaalikoodi
    try:
        for state in range(0, statelist_pituus):
            charlist = statelist[state].getElementsByTagName('char')
            merkki = charlist[0]
            heksa = merkki.getAttribute('code')
            #print "tila ", state, ": ", unichr(int(heksa, 16)).encode('utf-8')
            print "tila ", statelist[state].getAttribute('code'), ": ", unichr(int(heksa, 16)).encode('utf-8')
    except IndexError:
        # print "Tilaa '0' ei löytynyt!"
        pass
    print ""