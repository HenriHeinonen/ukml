# -*- coding: utf-8 -*-
from xml.dom import minidom
tiedosto = 'UNIdead.ukml'
print "Luetaan muistiin UKML-tiedosto ", tiedosto, "..."
doc = minidom.parse(tiedosto)

keylist = doc.getElementsByTagName('key')
print len(keylist), " näppäintä määritelty."

deadkeylist = doc.getElementsByTagName('deadkey')
print len(deadkeylist), " kuollutta näppäintä määritelty."

# Tulostetaan pykälämerkki: §
statelist = keylist[0].getElementsByTagName('state')
charlist = statelist[0].getElementsByTagName('char')
merkki = charlist[0]
heksa = merkki.getAttribute('code')
print unichr(int(heksa, 16)).encode('utf-8')

# Tulostetaan pitkä ajatusviiva: —
statelist = keylist[51].getElementsByTagName('state')
charlist = statelist[3].getElementsByTagName('char')
merkki = charlist[0]
heksa = merkki.getAttribute('code')
print unichr(int(heksa, 16)).encode('utf-8')