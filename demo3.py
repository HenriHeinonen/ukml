# -*- coding: utf-8 -*-
from xml.dom import minidom
import unicodedata
tiedosto = 'UNIdead.ukml'
print "Luetaan muistiin UKML-tiedosto ", tiedosto, "..."
doc = minidom.parse(tiedosto)

keylist = doc.getElementsByTagName('key')
keylist_pituus = len(keylist)
print keylist_pituus, " näppäintä määritelty."

deadkeylist = doc.getElementsByTagName('deadkey')
deadkeylist_pituus = len(deadkeylist)
print deadkeylist_pituus, " kuollutta näppäintä määritelty."
print ""

for key in range(0, keylist_pituus):
    statelist = keylist[key].getElementsByTagName('state')
    statelist_pituus = len(statelist)
    paikka = keylist[key].getAttribute('position')
    virtuaalikoodi = keylist[key].getAttribute('code')
    #print "paikka = ", paikka
    print "paikka = %s" % (paikka)
    #print "virtuaalikoodi = ", virtuaalikoodi
    print "virtuaalikoodi = %s" % (virtuaalikoodi)
    try:
        for state in range(0, statelist_pituus):
            charlist = statelist[state].getElementsByTagName('char')
            merkki = charlist[0]
            heksa = merkki.getAttribute('code')
            #print "tila ", state, ": ", unichr(int(heksa, 16)).encode('utf-8')
            unicodenimi = unicodedata.name(unicode(unichr(int(heksa, 16)).encode('utf-8'), "utf-8"))
            #print "tila ", statelist[state].getAttribute('code'), ": ", unichr(int(heksa, 16)).encode('utf-8'), " (",unicodenimi,")"
            print "tila ", statelist[state].getAttribute('code'), ": ", unichr(int(heksa, 16)).encode('utf-8'), " (U+",heksa, " ", unicodenimi,")"
            #print "tila %s: %s (%s)" % (statelist[state].getAttribute('code'), unichr(int(heksa, 16)).encode('utf-8'), unicodenimi)
            #print "tila %s: %s (U+%s %s)" % (statelist[state].getAttribute('code'), unichr(int(heksa, 16)).encode('utf-8'), heksa, unicodenimi)
    except IndexError:
        pass
    except ValueError:
        pass
    print "" # Tulostetaan tyhjä rivi, jotta lopputulos näyttäisi selkeämmältä