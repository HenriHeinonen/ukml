# -*- coding: utf-8 -*-
"""demo4.py esittelee, miten UKML-tiedostosta luetaan näppäintietoja 
ja niiden tilatietoja.
"""
from xml.dom import minidom
import struct

def unichar(i):
    # unichar-funktio on otettu täältä http://stackoverflow.com/a/28326717
    try:
        return unichr(i)
    except ValueError:
        return struct.pack('i', i).decode('utf-32')

def lue_ukml(tiedosto):
    """Jäsennetään UKML-tiedosto Minidomin avulla.
    
    Argumentti:
    tiedosto -- UKML-tiedosto
    """
    print "Luetaan muistiin UKML-tiedosto %s..." % (tiedosto)
    doc = minidom.parse(tiedosto)
    return doc

def lue_unicode(tiedosto):
    """Avataan tiedosto, joka sisältää Unicode-merkit selityksineen. 
    Luodaan dict-tyyppinen sanakirja, jonka avainsanana on Unicode-merkin heksakoodi. 
    Avainsanan avulla päästään käsiksi kyseisen Unicode-merkin selitykseen.
    
    Argumentti:
    tiedosto -- Tekstitiedosto, joka sisältää Unicode-tietouden: ftp://ftp.unicode.org/Public/UNIDATA/UnicodeData.txt
    """
    print "Luetaan muistiin Unicode-tiedosto %s..." % (tiedosto)
    unicodetiedosto = open(tiedosto)
    unicoderivit = len(unicodetiedosto.readlines())
    print "%s riviä Unicode-tiedostossa..." % (unicoderivit)
    print ""
    unicodesanakirja = {}
    unicodetiedosto.seek(0)
    for i in range(0, unicoderivit):
        rivi = unicodetiedosto.readline()
        lista = rivi.split(";")
        unicodesanakirja[lista[0]] = lista[1]
    return unicodesanakirja

def luo_tilanimet(*args):
    """Luodaan näppäimen tiloille nimet.
    Luodaan siis dict-tyyppinen sanakirja, jonka avainsanana on näppäintilan arvo, esim. 3.
    Avainsanan avulla päästään käsiksi kyseisen näppäintilan nimeen. Esimerkiksi, 3: "AltGr".
    
    Argumentit:
    *args -- Sisältää mielivaltaisen määrän argumentteja, joiden tulisi olla string-muotoisia lauseita, 
    esim. "Vaihto + AltGr". Argumenttien järjestyksellä on merkitystä, koska ensimmäinen argumentti saa 
    dict-sanakirjassa avainsanaksi arvon 0, toinen argumentti saa avainsanaksi arvon 1, jne.
    """
    tilanimet = {}
    for laskuri in range(0, len(args)):
        tilanimet[laskuri] = args[laskuri]
    return tilanimet

def koosta_nappaimet(doc):
    keylist = doc.getElementsByTagName('key')
    keylist_pituus = len(keylist)
    print "%s näppäintä määritelty." % (keylist_pituus)
    return keylist, keylist_pituus

def koosta_kuolleet_merkit(doc):
    deadkeylist = doc.getElementsByTagName('deadkey')
    deadkeylist_pituus = len(deadkeylist)
    print "%s kuollutta näppäintä määritelty." % (deadkeylist_pituus)
    print ""
    return deadkeylist, deadkeylist_pituus

def tulosta_kuolleet_merkit(deadkeylist, deadkeylist_pituus):
    for i in range(0, deadkeylist_pituus):
        #print deadkeylist[i].toxml()
        if (deadkeylist[i].hasChildNodes() == True):
            deadcharlist = deadkeylist[i].getElementsByTagName('char')
            deadcharlist_pituus = len(deadcharlist)
            for j in range(0, deadcharlist_pituus):
                if (deadcharlist[j].hasChildNodes() == True):
                    deadcharlist2 = deadcharlist[j].getElementsByTagName('char')
                    deadcharlist2_pituus = len(deadcharlist2)
                    #print deadcharlist2[0].toxml()
                    for k in range(0, deadcharlist2_pituus):
                        if (deadcharlist2[k].hasChildNodes() == True):
                            deadcharlist3 = deadcharlist2[k].getElementsByTagName('char')
                            deadcharlist3_pituus = len(deadcharlist3)
                            if (deadcharlist3[0].getAttribute('chained') != "yes"):
                                #print deadcharlist3[0].toxml()
                                heksa = deadcharlist3[0].getAttribute('code')
                                print unichar(int(heksa, 16)).encode('utf-8'), "(U+%s)" % (heksa),
            
    #print deadkeylist_pituus

def tulosta_nappaimet(tilanimet, keylist, keylist_pituus, unicodesanakirja):
    for key in range(0, keylist_pituus):
        statelist = keylist[key].getElementsByTagName('state')
        statelist_pituus = len(statelist)
        paikka = keylist[key].getAttribute('position')
        virtuaalikoodi = keylist[key].getAttribute('code')
        print "paikka = %s" % (paikka)
        print "virtuaalikoodi = %s" % (virtuaalikoodi)
        try:
            for state in range(0, statelist_pituus):
                charlist = statelist[state].getElementsByTagName('char')
                merkki = charlist[0]
                heksapituus = len(merkki.getAttribute('code').split()) # Tutkitaan, onko kyseessä yksi merkki vai ligatuuri
                tilanimi = tilanimet[int(statelist[state].getAttribute('code'))]
                for i in range(0, heksapituus):
                    heksa = merkki.getAttribute('code').split()[i]
                    unicodenimi = unicodesanakirja[heksa]
                    if (i==0):
                        print "tila = %s" % (tilanimi), ":", unichar(int(heksa, 16)).encode('utf-8'), "(U+%s %s)" % (heksa, unicodenimi), # Pilkku lopussa estää rivinvaihdon tulosteessa
                    else:
                        print unichar(int(heksa, 16)).encode('utf-8'), "(U+%s %s)" % (heksa, unicodenimi), # Pilkku lopussa estää rivinvaihdon tulosteessa
                        if (i==(heksapituus-1)):
                            print "[ligatuuri]",
                if (merkki.getAttribute('dead') == "yes"):
                    print "[kuollut merkki]",
                print "" # Tulostetaan lopuksi rivinvaihto
        except IndexError:
            pass
        except ValueError:
            pass
        print "" # Tulostetaan tyhjä rivi, jotta lopputulos näyttäisi selkeämmältä

def main():
    tilanimet = luo_tilanimet("perus","Vaihto","Ctrl","AltGr","Vaihto + AltGr","Vaihto + Kana","Kana","Ctrl + Kana","Vaihto + Ctrl","Vaihto + Ctrl + Kana","Kana + AltGr","Vaihto + Kana + AltGr")
    
    doc = lue_ukml("UNIdead3.ukml")

    keylist, keylist_pituus = koosta_nappaimet(doc)    
    deadkeylist, deadkeylist_pituus = koosta_kuolleet_merkit(doc)
        
    unicodesanakirja = lue_unicode("UnicodeData.txt")
    
    tulosta_nappaimet(tilanimet, keylist, keylist_pituus, unicodesanakirja)
    tulosta_kuolleet_merkit(deadkeylist, deadkeylist_pituus)

if __name__ == "__main__":
    main()